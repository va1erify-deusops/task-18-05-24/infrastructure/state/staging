# Creds
variable "token" {}
variable "cloud_id" {}
variable "folder_id" {}

# General
variable "zone" {
  default = "ru-central1-a"
}